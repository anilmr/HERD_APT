from fabric.api import run, env, put

env.hosts = ["anilmr@node2.herdapt.cs6480phantomnet-PG0.apt.emulab.net",
             "anilmr@node3.herdapt.cs6480phantomnet-PG0.apt.emulab.net",
             "anilmr@node4.herdapt.cs6480phantomnet-PG0.apt.emulab.net",
             "anilmr@node5.herdapt.cs6480phantomnet-PG0.apt.emulab.net",
             "anilmr@node6.herdapt.cs6480phantomnet-PG0.apt.emulab.net",
             "anilmr@node7.herdapt.cs6480phantomnet-PG0.apt.emulab.net",
             "anilmr@node8.herdapt.cs6480phantomnet-PG0.apt.emulab.net",
             "anilmr@node9.herdapt.cs6480phantomnet-PG0.apt.emulab.net",
             "anilmr@node10.herdapt.cs6480phantomnet-PG0.apt.emulab.net",
             "anilmr@node11.herdapt.cs6480phantomnet-PG0.apt.emulab.net",
             "anilmr@node12.herdapt.cs6480phantomnet-PG0.apt.emulab.net",
             "anilmr@node13.herdapt.cs6480phantomnet-PG0.apt.emulab.net",
            ]

last_octet = 1

def setup_ipmi():
    run("sudo apt-get -y install libgcrypt11-dev")
    run("wget http://ftp.gnu.org/gnu/freeipmi/freeipmi-1.4.11.tar.gz")
    run("tar xzf freeipmi-1.4.11.tar.gz")
    run("cd freeipmi-1.4.11 && sudo ./configure && sudo make && sudo make install")

def assign_ip():
    global last_octet
    run("sudo ifconfig ib0 10.10.10." + str(last_octet) + "/24 up")
    last_octet += 1

def check_ofed():
    run("sudo ofed_info | head -1")

def kill_exp():
    run("cd /users/anilmr/project/HERD && sudo bash local-kill.sh")

def delete():
    run("sudo rm -rf project")

def restart():
    run("sudo init 6")

def ofed_download():
    run("wget http://www.mellanox.com/downloads/ofed/MLNX_OFED-3.1-1.0.3/MLNX_OFED_LINUX-3.1-1.0.3-ubuntu12.04-x86_64.tgz")

def install_ofed():
    run('tar -xzf MLNX_OFED_LINUX-3.1-1.0.3-ubuntu12.04-x86_64.tgz')
    run('cd MLNX_OFED_LINUX-3.1-1.0.3-ubuntu12.04-x86_64 && sudo ./mlnxofedinstall --force')

def configure_hugepages():
    run("sudo sysctl -w vm.nr_hugepages=4096")
    run("cat /proc/meminfo |grep HugePages")

def host_info():
    run('cat /etc/issue && uname -a')

def update_apt():
    run("sudo apt-get -y update")

def install_java():
    run("sudo apt-get -y install default-jdk")

def copy_file():
    put('load.sh','/users/anilmr')
    put('run_server.sh','/users/anilmr/project/HERD')

def check_rdmacm_modules():
    run('modinfo rdma_cm | head -1')

def load_modules():
    run('sudo sh load.sh')

def check_modules():
    run('lsmod | grep rdma_ucm')

def check_ibvdevices():
    run('ibv_devices')

def install_git():
    run('sudo apt-get install git')

def copy_project():
    install_git()
    run('mkdir project && cd project && rm -rf HERD && git clone https://github.com/anilmr/HERD.git && cd HERD && make clean && make')

def install_project():
    run('cd project/HERD && make clean && make')

def copy_server_file():
    put('/Users/anil/Research/project_apt/servers','/users/anilmr/project/HERD')

def init_shm():
    run("sudo sysctl -w kernel.shmmax=2147483648 && sudo sysctl -w kernel.shmall=2147483648 && sudo sysctl -p /etc/sysctl.conf")

def rping():
    run("rping -c -a 10.10.10.1 -C1")

def configure_zipf():
    run("cd project/HERD/YCSB/src/ && sudo rm -rf /dev/zipf && sudo mkdir /dev/zipf && sudo java zipf")

