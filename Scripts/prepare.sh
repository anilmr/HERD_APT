#!/bin/bash

nodes=(apt168 apt152 apt129 apt183 apt159 apt161 apt142 apt138 apt167 apt184 apt166 apt179 apt149 apt133 apt156 apt162 apt126 apt130)
username=anilmr

for i in ${nodes[@]};
do
	ssh ${username}@${i}.apt.emulab.net hostname
	ssh -oStrictHostKeyChecking=no ${username}@${i}.apt.emulab.net "rm -rf HERD && git clone https://github.com/anilmr/HERD.git && cd HERD && make clean && make"
	scp run_client.sh servers ${username}@${i}.apt.emulab.net:/users/anilmr/HERD/
done
