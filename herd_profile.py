import geni.portal as portal
import geni.rspec.pg as pg
 
pc = portal.Context()
rspec = pg.Request()
 
nodes = ["node"+str(i) for i in range(1,19)]
ifaces = ["iface"+str(i) for i in range(1,19)]

i = 0
for node in nodes:
	node = pg.RawPC(node)
	node.hardware_type = "r320"
    node.disk_image = "https://www.apt.emulab.net/image_metadata.php?uuid=841c55ca-e9f1-11e4-8b63-2f7555356a5c"
	ifaces[i] = node.addInterface("if" + str(i))
	i+=1
	rspec.addResource(node)
	

link = pg.LAN("lan")
for iface in ifaces:
	link.addInterface(iface)

rspec.addResource(link)
pc.printRequestRSpec(rspec)
